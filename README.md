## Veracity ##

Distributed revision control system written by SourceGear LLC.
It is implemented in C and JS, licensed under Apache v2. Latest stable is 2.5, spring 2013.
Official site hasn't been been updated since.